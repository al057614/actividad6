/**
 * @author UNIVERSIDAD AUTONOMA DE CAMPECHE
 * @author ISC
 * @author William Manuel Vargas Cetz
 */

package pilasalgebraicas;

import java.util.Stack;

public class PilasAlgebraicas {

    public static void main(String[] args) {
       
        
        String cadena = "(Cadena () (()(())))";

        String cadena2 = "(Cadena )(()()))";
        
        String cadena3 = "(Cadena () (()(()))) ";
        
        String cadena4 = "(Cadena )(()())) ";
        
        String cadena5 = "(Cadena )(()())) ";

        System.out.println("(123*4)+((12-1)*(1+2))");

        System.out.println(verificaParentesis(cadena));
          
        System.out.println("(123*4)+((12-1)*+1+2))");
            
        System.out.println(verificaParentesis(cadena2));
           
        System.out.println("((pila.size()==0)&&(flag))");
            
        System.out.println(verificaParentesis(cadena3));
        
        System.out.println("((pila.size)==0)&&(flag))");
            
        System.out.println(verificaParentesis(cadena4));
        
        System.out.println("((pila.size()==0)&&(flag)");
            
        System.out.println(verificaParentesis(cadena5));
            

    }

 

    public static boolean verificaParentesis(String cadena)  {

        Stack<String> pila = new Stack<>();       int i = 0;

            while (i<cadena.length()) {  

                if(cadena.charAt(i)=='(') {pila.push("(");}                                

                else if  (cadena.charAt(i)==')') {  

                            if (!pila.empty()){ pila.pop(); } 

                            else { pila.push(")"); break; } 

                }

                i++;

            }

        return pila.empty();
        
        
    }
    
}
